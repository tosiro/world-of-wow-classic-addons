# World of WoW Classic Addons

Collection of addons for World of WoW Classic Addons

### Last check: 2019-10-18

### AddOns:

AddOn | Version | Last-Updated
----- | ------- | ------------
[AtlasLootClassic](https://www.curseforge.com/wow/addons/atlaslootclassic) | v1.3.1-classic | 2019-10-18
[Auctionator](https://www.curseforge.com/wow/addons/auctionator) | 8.2.0 | 2019-10-18
[Bagnon](https://www.curseforge.com/wow/addons/bagnon) | 8.2.16 | 2019-10-18
[HealBot Continued](https://www.curseforge.com/wow/addons/heal-bot-continued) | 1.13.2.7 | 2019-10-18
[ClassicThreatMeter](https://www.curseforge.com/wow/addons/classicthreatmeter) | 1.10 | 2019-10-18
[Classic Quest Log for Classic](https://www.wowinterface.com/downloads/info24937-ClassicQuestLogforClassic.html) | 1.4.6-Classic | 2019-10-18
[Deadly Boss Mods (DBM)](https://www.curseforge.com/wow/addons/deadly-boss-mods) | 1.3.16-classic | 2019-10-18
[DejaClassicStats](https://www.curseforge.com/wow/addons/dejaclassicstats) | 1302r030 | 2019-10-18
[Details!](https://www.curseforge.com/wow/addons/details-damage-meter-classic-wow) | 1.13.2.168.140 | 2019-10-18
[ElvUI](https://www.tukui.org/classic-addons.php?id=2) | 1.14 | 2019-10-18
[GatherLite](https://www.curseforge.com/wow/addons/gatherlite) | 1.3.3-classic | 2019-10-18
[Questie](https://www.curseforge.com/wow/addons/questie) | v4.3.0 | 2019-10-18
[Real Mob Health](https://www.curseforge.com/wow/addons/real-mob-health) | 2.13 | 2019-10-18
[Scrap (Junk Seller)](https://www.curseforge.com/wow/addons/scrap) | 13.19 | 2019-10-18
[Titan Panel Classic](https://www.curseforge.com/wow/addons/titan-panel-classic) | 1.0.7.11302 | 2019-10-18
[~~Tukui~~](https://www.tukui.org/classic-addons.php?id=1) | Replaced by | [ElvUI](https://www.tukui.org/classic-addons.php?id=2)
[Vendor Price](https://www.curseforge.com/wow/addons/vendor-price) | 1.2.9-classic | 2019-10-18
